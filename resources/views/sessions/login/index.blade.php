@extends ('sessions/template/template')

<title>Login</title>
@section('content')

@if(count($errors) > 0)
    <div class="alert alert-danger" roler="alert">
        
        @foreach ($errors->all() as $error)
       	{{$error}} <br>
        @endforeach
                               
    </div>
@endif

<div class="container-fluid">


	{!! Form::open(array('url' => 'home', 'method' => 'POST', 'class' => 'form-signin')) !!}
	<h1 class="form-signin-heading">Login</h1>

	<p>
		{!! $errors->first('username') !!}
		{!! $errors->first('password') !!}
	</p>

	<p>
		{!! Form::label('username', 'Username', array('class' => 'form-signin')) !!}
		{!! Form::text('username', Input::old('username'), array('placeholder'=>'Username', 'required', 'class' => 'form-control')) !!}
	</p>

	<p>
		{!! Form::label('password', 'Password', array('class' => 'form-signin')) !!}
		{!! Form::password('password', array('placeholder'=>'Password', 'required', 'class' => 'form-control')) !!}
	</p>

	<p>
		{!! Form::submit('Login', array('class' => 'btn btn-lg btn-primary btn-block')) !!}
	</p>

	{!! Form::close() !!}

</div>

@stop
