@extends('sessions/layout/template')

@section('content')
    <h1>Edit Customer Information</h1>
    {!! Form::model($customer,['method' => 'PATCH','route'=>['customers.update',$customer->id]]) !!}
    <div class="form-group">
        {!! Form::label('Customer Name', 'Customer Name:') !!}
        {!! Form::text('cust_name',null,['class'=>'form-control']) !!}
    </div>
    <div class="form-group">
        {!! Form::label('Contact Number', 'Contact Number:') !!}
        {!! Form::text('contact_no',null,['class'=>'form-control']) !!}
    </div>
    <div class="form-group">
        {!! Form::label('Email Address', 'Email Address:') !!}
        {!! Form::text('email_add',null,['class'=>'form-control']) !!}
    </div>
    <div class="form-group">
        {!! Form::label('Address', 'Address:') !!}
        {!! Form::text('address',null,['class'=>'form-control']) !!}
    </div>
   
    <div class="form-group">
        {!! Form::submit('Update', ['class' => 'btn btn-primary']) !!}
    </div>
    {!! Form::close() !!}
@stop