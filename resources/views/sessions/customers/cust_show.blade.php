@extends('sessions/layout/template')

@section('content')
    <h1>Customer Information</h1>

    <form class="form-horizontal">
        <div class="form-group">
            <label>Customer Name</label>
            <div class="col-sm-10">
                <p class="form-control"> {{$customer->cust_name}}</p>
            </div>
        </div>
        <div class="form-group">
            <label>Contact Number</label>
            <div class="col-sm-10">
                <p class="form-control"> {{$customer->contact_no}}</p>
            </div>
        </div>
        <div class="form-group">
            <label>Email Address</label>
            <div class="col-sm-10">
                <p class="form-control"> {{$customer->email_add}}</p>
            </div>
        </div>
        <div class="form-group">
            <label>Address</label>
            <div class="col-sm-10">
                <p class="form-control">{{$customer->address}}</p>
            </div>
        </div>

        <div class="form-group">
            <div class="col-sm-offset-2 col-sm-10">
                <a href="{{ url('customers')}}" class="btn btn-primary">Back</a>
            </div>
        </div>
    </form>
@stop