@extends('sessions/layout/template')

@section('content')
    <h1>Add Customer</h1>
    @if($errors->any())
    <div class="alert alert-danger">
        @foreach($errors->all() as $error)
            <p>{{ $error }}</p>
        @endforeach
    </div>
    @endif

    @if(Session::has('flash_message'))
    <div class="alert alert-success">
        {{ Session::get('flash_message') }}
    </div>
    @endif
    
    {!! Form::open(['url' => 'customers']) !!}
    <div class="form-group">
        {!! Form::label('Customer Name', 'Customer Name:') !!}
        {!! Form::text('cust_name',null,['class'=>'form-control']) !!}
    </div>
    <div class="form-group">
        {!! Form::label('Contact Number', 'Contact Number:') !!}
        {!! Form::text('contact_no',null,['class'=>'form-control']) !!}
    </div>
    <div class="form-group">
        {!! Form::label('Email Address', 'Email Address:') !!}
        {!! Form::text('email_add',null,['class'=>'form-control']) !!}
    </div>
    <div class="form-group">
        {!! Form::label('Address', 'Address:') !!}
        {!! Form::text('address',null,['class'=>'form-control']) !!}
    </div>
    
    <div class="form-group">
        {!! Form::submit('Save', ['class' => 'btn btn-primary form-control']) !!}
    </div>
        <div>
                <a href="{{ url('customers')}}" class="btn btn-primary form-control">Cancel</a>
            </div>
    {!! Form::close() !!}
@endsection