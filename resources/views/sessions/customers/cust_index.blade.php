@extends('sessions.layout.template')

@section('content')
 <div class = "row">
        <!-- LIST OF CUSTOMERS -->
        <div class = "col s12">
            <div class = "table-card">
                <div class = "card">
                    <div class = "card-header red lighten-2">
                        <div class="card-title">
                            <h6 class="table-card-title"><i class="mdi mdi-store"></i> &nbsp LIST OF CUSTOMERS</h6>
                        </div>
                    </div>
                    <div class = "card-content">                            
                        <!-- Buttons -->
                        <div class = "col s12 space">
                            <!-- Add -->
                            <a class="waves-effect waves-light green btn" onclick="location.href='{{url('/customers/create')}}';"><i class="mdi mdi-plus"></i> Add</a>
                                        <!-- Edit -->
                                        <a class="waves-effect waves-light orange btn" onclick="location.href = '{{url('/customers/edit')}}';"><i class="mdi mdi-pencil"></i></i>Edit</a>
                                        <!-- Delete -->
                                        <a id = "delete" class="waves-effect waves-light red btn" onclick="Materialize.toast('Data Deleted', 4000)"><i class="mdi mdi-delete-variant"></i>Delete</a>
                                    </div>
                                    <table id="cusTable" class="display compact nowrap" cellspacing="0" width="100%">
                                        <thead>
                                            <tr>
                                                <th>ID</th>
                                                <th>Customer Code</th>
                                                <th>Short Name</th>
                                                <th>Customer Name</th>
                                                <th>Address 1</th>
                                                <th>Telephone 1</th>
                                                <th>Email Address</th>
                                            </tr>
                                        </thead>

                                        <tbody>
                                            @foreach ($customers as $customer)
                                            <tr>
                                                <td>{{ $customer->id }}</td>
                                                <td>{{ $customer->cust_code }}</td>
                                                <td>{{ $customer->cust_short }}</td>
                                                <td>{{ $customer->cust_name }}</td>
                                                <td>{{ $customer->address }}</td>
                                                <td>{{ $customer->contact_no }}</td>
                                                <td>{{ $customer->email_add }}</td>
                                            </tr>
                                            @endforeach
                                        </tbody>

                                        <tfoot>
                                            <tr>
                                                <th>ID</th>
                                                <th>Customer Code</th>
                                                <th>Short Name</th>
                                                <th>Customer Name</th>
                                                <th>Address 1</th>
                                                <th>Telephone 1</th>
                                                <th>Email Address</th>
                                            </tr>
                                        </tfoot>
                                    </table>
                                </div>
                                <!--Temporary! Remove this when data is ready -->
                                <div class = "section"></div>
                            </div>
                        </div>
                    </div>
                </div>
@endsection