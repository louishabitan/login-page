@extends('sessions.home.master')

@section('content')

<title>Mitsukoshi Motors</title>

</head>
<body>

	<div>
		<h1>WELCOME!</h1>
	</div>

	<div>
		<h2><a href="{{url('/suppliers')}}" class="btn btn-info">Suppliers</a></h2>
		<h2><a href="{{url('/customers')}}" class="btn btn-info">Customers</a></h2>
		<h2><a href="{!! URL::to('logout') !!}" class="btn btn-info">Logout</a></h2>
	</div>

@endsection