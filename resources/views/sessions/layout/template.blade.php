<!doctype html>
<html lang="en">
	<head>
		<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
		<meta name="viewport" content="width=device-width, initial-scale=1">
		<meta http-equiv="X-UA-Compatible" content="IE=edge">
		<meta name="msapplication-tap-highlight" content="no">
		
		<meta name="description" content="Mitsukoshi Motors Philippines Inc. Warehouse Management System">
		
		<title>Mitsukoshi Motors Philippines Inc. - Customer</title>
		
		<!-- Icons-->
		<link rel="shortcut icon" type="image/png" href="images/others/favicon.png"/>
		

		<!-- CSS-->
		
		<link href="css/prism.css" rel="stylesheet">
		<link href="css/ghpages-materialize.css" type="text/css" rel="stylesheet">
		<link href="css/style.css" type="text/css" rel="stylesheet">
		<link href="MaterialDesign/css/materialdesignicons.min.css" media="all" rel="stylesheet" type="text/css" />
		
		
		<!-- CSS for charts and tables  -->
		<link href="bower_components/datatables-responsive/css/dataTables.responsive.css" type="text/css" rel="stylesheet">
		<link href="bower_components/datatables/media/css/jquery.dataTables.css" type="text/css" rel="stylesheet">
		<link href="bower_components/datatables-tabletools/css/dataTables.tableTools.css" type="text/css" rel="stylesheet">
		
	</head>

	<body>
		<header>
			<ul id="nav-mobile" class="side-nav fixed">
				<li class="logo">
					<a id="logo-container" href="index.html" class="brand-logo">
						<object id="front-page-logo" type="image/png" data="res/mitsukoshi.png">Your browser does not support PNG</object>
					</a>
				</li>
				<li class="bold"><a href="index.html" class="waves-effect waves-red"><i class="mdi mdi-view-dashboard"></i>&nbsp &nbsp &nbsp Dashboard</a></li>
				<li class="no-padding">
					<ul class="collapsible collapsible-accordion">
						<li class="bold"><a class="collapsible-header waves-effect waves-red"><i class="mdi mdi-truck"></i>&nbsp &nbsp Suppliers</a>
							<div class="collapsible-body">
								<ul>
									  <li><a href="gen_supplier.html">General</a></li>
									  <li><a href="add_supplier.html">Add New Supplier Record</a></li>
								</ul>
							</div>
						</li>
						<li class="bold"><a class="collapsible-header active waves-effect waves-red"><i class="mdi mdi-store"></i>&nbsp &nbsp Customers</a>
							<div class="collapsible-body">
								<ul>
									  <li class = "active"><a href="gen_customer.html">General</a></li>
									  <li><a href="add_customer.html">Add New Customer Record</a></li>
								</ul>
							</div>
						</li>
						<li class="bold"><a class="collapsible-header  waves-effect waves-red"><i class="mdi mdi-package-variant"></i>&nbsp &nbsp Inventory</a>
							<div class="collapsible-body">
								<ul>
									  <li><a href="gen_inventory.html">General</a></li>
									  <li><a href="add_inventory.html">Add New Product Record</a></li>
								</ul>
							</div>
						</li>
						<li class="bold"><a class="collapsible-header  waves-effect waves-red"><i class="mdi mdi-account-multiple"></i>&nbsp &nbsp Accounts</a>
							<div class="collapsible-body">
								<ul>
									  <li><a href="gen_account.html">General</a></li>
									  <li><a href="add_account.html">Add New Account</a></li>
								</ul>
							</div>
						</li>
					</ul>
				</li>
				<li class="bold"><a href="reports.html" class="waves-effect waves-red"><i class="mdi mdi-file-outline"></i>&nbsp &nbsp &nbsp Reports</a></li>
			</ul>
			
			<ul id="second" class="side-nav buttonfixed">
				<li class="blue-grey darken-2 z-depth-2" style = "padding-top:25px; padding-bottom:20px;">
						<img src="images/users/avatar-001.jpg" alt="" style = "margin-left: 76px;" witdh="65" height="65"  class=" circle responsive-image ">	
						<h5 class = "grey-text text-lighten-5 center-align">Jon Snow</h5>
						<h6 class = "grey-text text-lighten-5 center-align">Supervisor</h6>
				</li>
				<li style = "margin-top:15px;"></li>
				<li class = "bold"><a href="#" class="waves-effect waves-red"><i class="mdi mdi-account-box"></i>&nbsp &nbsp &nbsp Profile</a></li>
				<li class = "bold"><a href="#" class="waves-effect waves-red"><i class = "mdi-action-settings-applications"></i>&nbsp &nbsp &nbsp Settings</a> </li>
				<li class = "bold"><a href="#" class="waves-effect waves-red"><i class = "mdi-action-exit-to-app"></i>&nbsp &nbsp &nbsp Logout</a></li>
				
			</ul>

			<div class="navbar-fixed">
				<nav>
					<div class="nav-wrapper blue-grey darken-4">
						<a href="#" class="brand-logo show-on-med-and-down hide-on-large-only">MITSUKOSHI</a>
						<a href="#" data-activates="nav-mobile" class="button-collapse top-nav waves-effect waves-light circle hide-on-large-only"><i class="mdi-navigation-menu"></i></a>
						<a href="#" data-activates="second" id="second-button" class="button-collapse2 top-nav show-on-large waves-effect waves-light circle "  style = "margin-top:3px;"><img src = "images/users/avatar-001.jpg" height = "32px" width = "32px" class = "circle responsive-image"></i></a>
					</div>
				</nav>
			</div>
		</header>

		<!--                           MAIN CONTENT                        -->
		<main>
    		<div class="container">
        
        		@yield('content')
    	
    		</div>
    	</main>
    	<!--                           END OF MAIN                         -->

		<footer class="page-footer blue-grey darken-4">		
			<div class="footer-copyright blue-grey darken-3">
				<div class="container center-align blue-grey darken-3">
					© 2015-2016 Team-ba Web Dev Group || UST-ISD
				</div>
			</div>
		</footer>
		
		<!--  Scripts -->
		<script src="dist/js/jquery-2.1.2.min.js"></script>
		
		
		

		<!-- Right SideNav Script -->
		<script >
			$( document ).ready(function() {
			$('#second-button').sideNav({ edge: 'right' });
			});

		</script>

		<!-- Table Major Script -->
		<script src="bower_components/datatables/media/js/jquery.dataTables.min.js"></script>
		<script src="bower_components/datatables-responsive/js/dataTables.responsive.js"></script>
		<script src="bower_components/datatables-tabletools/js/dataTables.tableTools.js"></script>
		
		<!-- Table Running Script -->
		<script src = "jstable/cusTable.js"></script>

		
		<!-- Other Scripts -->
		<script src="js/jquery.timeago.min.js"></script> 
		<script src="js/prism.js"></script>
		<script src="dist/js/materialize.min.js"></script>
		<script src="js/init.js"></script>
	</body>
</html>