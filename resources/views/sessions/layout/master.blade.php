<!doctype html>
<html lang="en">
<head>
<meta charset="UTF-8">
<title>Mitsukoshi Motors</title>
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.4/css/bootstrap.min.css">
<link href="css/prism.css" rel="stylesheet">
<link href="css/ghpages-materialize.css" type="text/css" rel="stylesheet">
<link href="css/style.css" type="text/css" rel="stylesheet">
<link href="MaterialDesign/css/materialdesignicons.min.css" media="all" rel="stylesheet" type="text/css" />
</head>
<body>
    <div class="container">
        @yield('content')
    </div>
</body>
</html>