$(document).ready(function() {

     var table = $('#invTable').DataTable();
 
    $('#invTable tbody').on( 'click', 'tr', function () {
        if ( $(this).hasClass('selected') ) {
            $(this).removeClass('selected');
        }
        else {
            table.$('tr.selected').removeClass('selected');
            $(this).addClass('selected');
        }
    } );
	
	new $.fn.dataTable.Responsive( table );
	
} );

$('#invTable').DataTable( {
	"dom": 'T<"clear">lfrtip',
	"tableTools": {
		"aButtons": [
			"copy",
			"print",
			{
				"sExtends":    "collection",
				"sButtonText": "Save",
				"aButtons":    [ "csv", "xls", "pdf" ]
			}
		]
	}
} );