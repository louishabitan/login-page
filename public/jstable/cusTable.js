$(document).ready(function() {

    var table = $('#cusTable').DataTable();
	
	$('#cusTable tbody').on( 'click', 'tr', function () {
        $(this).toggleClass('selected');
        var d = table.row(this).index();
    } );
 
    $('#delete').click( function () {
        table.rows('.selected').remove().draw( false ) ;
    } );
	
	
	new $.fn.dataTable.Responsive( table );
	
} );

$('#cusTable').DataTable( {
	"dom": 'T<"clear">lfrtip',
	"tableTools": {
		"aButtons": [
			"copy",
			"print",
			{
				"sExtends":    "collection",
				"sButtonText": "Save",
				"aButtons":    [ "csv", "xls", "pdf" ]
			}
		]
	}
} );