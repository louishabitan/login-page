$(document).ready(function() {

    var table = $('#accTable').DataTable();
	
	$('#accTable tbody').on( 'click', 'tr', function () {
        $(this).toggleClass('selected');
    } );
 
    $('#delete').click( function () {
        table.rows('.selected').remove().draw( false ) ;
    } );
	
	
	new $.fn.dataTable.Responsive( table );
	
} );

$('#accTable').DataTable( {
	"dom": 'T<"clear">lfrtip',
	"tableTools": {
		"aButtons": [
			"copy",
			"print",
			{
				"sExtends":    "collection",
				"sButtonText": "Save",
				"aButtons":    [ "csv", "xls", "pdf" ]
			}
		]
	}
} );