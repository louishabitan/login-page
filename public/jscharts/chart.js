
var app = angular.module("sales", ["chart.js"]);

//** Sales Growth **//
app.controller("myController", function ($scope) {

  $scope.labels = ["January", "February", "March", "April", "May", "June", "July", "August", "September", "October", "November", "December"];
  $scope.series = ['SYM', 'MAKOTO', 'LIFAN'];
  $scope.data = [
    [65, 59, 80, 81, 56, 55, 40, 1, 56, 5, 20, 30],
    [28, 48, 40, 19, 86, 27, 90, 56, 55, 40, 1,],
	[28, 65, 59, 80, 81, 86, 27, 90, 81, 56, 55,3]
  ];
  $scope.onClick = function (points, evt) {
    console.log(points, evt);
  };
});

//** Gen Sales **//
app.controller("salesCtrl", function ($scope) {
  $scope.labels = ["SYM", "MAKOTO", "LIFAN"];
  $scope.series = ['Past', 'Current'];
  $scope.data = [
  [200, 600, 500],
  [300, 500, 100]
  ];
});

//** Product Mix **//
app.controller("mixCtrl", function ($scope) {
  $scope.labels = ["nails", "screws", "headlights"];
  $scope.data = [300, 500, 100];
});

//** SYM WAREHOUSE **//
app.controller("symPolar", function ($scope) {
  $scope.labels = ["In-Store Sales", "Mail-Order Sales", "Branch Sales"];
  $scope.data = [300, 500, 100];
});

          
//** MAKOTO WAREHOUSE **//     

app.controller("mktPolar", function ($scope) {
  $scope.labels = ["In-Store Sales", "Mail-Order Sales", "Branch Sales"];
  $scope.data = [300, 500, 100];
});

//** LIFAN WAREHOUSE **//     

app.controller("lfanPolar", function ($scope) {
  $scope.labels = ["In-Store Sales", "Mail-Order Sales", "Branch Sales"];
  $scope.data = [300, 500, 100];
});

        