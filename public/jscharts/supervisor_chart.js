
var app = angular.module("warehouse", ["chart.js"]);

//** Sales Growth **//
app.controller("myController", function ($scope) {

  $scope.labels = ["January", "February", "March", "April", "May", "June", "July", "August", "September", "October", "November", "December"];
  $scope.series = ['Warehouse'];
  $scope.data = [
    [65, 59, 80, 81, 56, 55, 40, 1, 56, 5, 20, 30]
  ];
  $scope.onClick = function (points, evt) {
    console.log(points, evt);
  };
});

//** Week Sales **//
app.controller("salesCtrl", function ($scope) {
  $scope.labels = ["W1","W2","W3","W4"];
  $scope.series = ['W1', 'W2', 'W3', 'W4'];
  $scope.data = [
  [200, 600, 500,21],
  [300, 500, 100,11],
  [50, 20, 11,15],
  [65, 30, 22,200]
  ];
});

//** Product Mix **//
app.controller("mixCtrl", function ($scope) {
  $scope.labels = ["nails", "screws", "headlights"];
  $scope.data = [300, 500, 100];
});

//** SYM WAREHOUSE **//
app.controller("warehousePolar", function ($scope) {
  $scope.labels = ["In-Store Sales", "Mail-Order Sales", "Branch Sales"];
  $scope.data = [300, 500, 100];
});

