<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Items extends Model
{
    protected $fillable=[
        'supplier_name',
        'contact_no',
        'email_add',
        'address'
    ];
}
