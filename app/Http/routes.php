<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/

Route::get('/', function () {
    return view('sessions/login/index');
});

Route::post('home', array('uses' => 'UsersController@store'));
Route::get('home', 'UsersController@index');
Route::get('logout', array('uses' => 'UsersController@destroy'));

Route::resource('customers', 'CustomerController');
Route::resource('suppliers','SupplierController');
Route::get('sessions/suppliers/{suppliers}', 'SupplierController@show');
Route::get('sessions/customers/{customers}', 'CustomerController@show');
Route::any('sessions/suppliers/{id}/edit','SupplierController@edit');
Route::any('sessions/customers/{id}/edit','CustomerController@edit');
