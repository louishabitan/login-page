<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use Session;
use App\Customer;
use App\Http\Requests;
use App\Http\Controllers\Controller;

class CustomerController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function index()
    {
        $customers = Customer::all();
        return view('sessions.customers.cust_index');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return Response
     */
    public function create()
    {
        return view('sessions.customers.cust_create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  Request  $request
     * @return Response
     */
    public function store(Request $request)
    {
        $this->validate($request, [
        'cust_code' => 'required',
        'cust_short' => 'required',
        'cust_name' => 'required',
        'address' => 'required',        
        'cust_name' => 'required',
        'contact_no' => 'required',
        'email_add' => 'required',
        'address' => 'required'
        ]);

        $customer=$request->all();
        Customer::create($customer);
        Session::flash('flash_message', 'Customer successfully added!');
        return redirect('customers');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return Response
     */
    public function show($id)
    {
        $customer = Customer::find($id);
        return View('sessions.customers.cust_show')
            ->with('customer', $customer);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return Response
     */
    public function edit($id)
    {
        $customer=Customer::findOrFail($id);
        return view('sessions.customers.cust_edit')->withCustomer($customer);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  Request  $request
     * @param  int  $id
     * @return Response
     */
    public function update(Request $request, $id)
    {
         $customer=Customer::findOrFail($id);

         $this->validate($request, [
        'cust_name' => 'required',
        'contact_no' => 'required',
        'email_add' => 'required',
        'address' => 'required'
        ]);

        $input = $request->all();
        $customer->fill($input)->save();
        Session::flash('flash_message', 'Customer successfully updated!');
        return redirect('customers');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return Response
     */
    public function destroy($id)
    {
        Customer::find($id)->delete();
        Session::flash('flash_message', 'Customer successfully deleted!');
        return redirect('customers');
    }
}
