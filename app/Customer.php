<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Customer extends Model
{
        protected $fillable=[
        'cust_code',
        'cust_short',
        'cust_name',
        'contact_no',
        'email_add',
        'address',
        'address_2',
        'cust_city',
        'cust_state',
        'cust_postal',
        'email_add'
    ];
}
